﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Boid : MonoBehaviour
{
	[HideInInspector] public bool ShouldAvoidWalls;
	public Vector3? Goal;
	public Cell CurrentCell { get; set; }
	public Vector3 Velocity { get; private set; }

	private GameController _gameController;

	private Vector3 _acceleration;
	private Transform _transform;
	private Renderer[] _renderers;

	/// <summary>
	/// Used for the AvoidWalls method, this way not every wall needs its own new instance of Vector3.
	/// </summary>
	private Vector3 _updatingVector;

	private void Start()
	{
		_gameController = GameController.Instance;
		_acceleration = new Vector3();
		_updatingVector = new Vector3();

		_transform = transform;
		_transform.position = new Vector3(
			Random.value * _gameController.WorldSize.x - _gameController.WorldSize.x / 2,
			Random.value * _gameController.WorldSize.y - _gameController.WorldSize.y / 2,
			Random.value * _gameController.WorldSize.z - _gameController.WorldSize.z / 2);

		Velocity = new Vector3(Random.value * 2 - 1, Random.value * 2 - 1, Random.value * 2 - 1);

		_renderers = GetComponentsInChildren<Renderer>();
		float color = Random.value;
		foreach (var r in _renderers)
		{
			r.material.color = new Color(color, color, color);
		}
	}

	public void Run()
	{
		var boids = _gameController.Graph.GetClosestBoids(_transform.position, _gameController.NeighborhoodRadius);

		if (ShouldAvoidWalls)
		{
			AvoidWalls();
		}

		if (Random.value > .5f)
		{
			Flock(boids);
		}

		Move();

		SetHeading();
		SetColorBasedOnDepth();
	}

	private void AvoidWalls()
	{
		AvoidWall(-_gameController.WorldSize.x, _transform.position.y, _transform.position.z);
		AvoidWall(_gameController.WorldSize.x, _transform.position.y, _transform.position.z);
		AvoidWall(_transform.position.x, -_gameController.WorldSize.y, _transform.position.z);
		AvoidWall(_transform.position.x, _gameController.WorldSize.y, _transform.position.z);
		AvoidWall(_transform.position.x, _transform.position.y, -_gameController.WorldSize.z);
		AvoidWall(_transform.position.x, _transform.position.y, _gameController.WorldSize.z);
	}

	private void AvoidWall(float x, float y, float z)
	{
		_updatingVector.Set(x, y, z);
		_updatingVector = Behaviours.Avoid(_updatingVector, _transform.position);
		_updatingVector *= 5;
		_acceleration += _updatingVector;
	}

	private void SetHeading()
	{
		_transform.forward = Velocity;
	}

	private void SetColorBasedOnDepth()
	{
		float color = (500 - _transform.position.z) / 1000;
		foreach (var r in _renderers)
		{
			r.material.color = new Color(color, color, color);
		}
	}

	#region Steering behaviours

	private void Flock(IEnumerable<Boid> boids)
	{
		if (Goal.HasValue)
		{
			_acceleration += Behaviours.Reach(_transform.position, Goal.Value, .005f);
		}

		var enumerable = boids as Boid[] ?? boids.ToArray();
		_acceleration += Behaviours.Alignment(enumerable, _gameController.MaxSteerForce) * _gameController.AllignmentWeight;
		_acceleration += Behaviours.Cohesion(enumerable, _transform.position, _gameController.MaxSteerForce) *
		                 _gameController.CohesionWeight;
		_acceleration += Behaviours.Separation(enumerable, _transform.position) * _gameController.SeparationWeight;
	}

	private void Move()
	{
		Velocity = Vector3.ClampMagnitude(Velocity + _acceleration, _gameController.MaxSpeed);
		_transform.position += Velocity;
		_acceleration = Vector3.zero;
	}

	public void Repulse(Vector3 target)
	{
		_acceleration += Behaviours.Repulse(target, _transform.position, 150);
	}

	#endregion
}