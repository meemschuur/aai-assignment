﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ExploreBehaviour : MonoBehaviour
{
	private GameController _gameController;

	private GameObject _targetCube;
	private Cell _targetCell;
	public Vector3 TargetPosition { get; private set; }

	private AStar _aStar;
	private Stack<Node> _currentPath;
	private Vector3Int _currentTargetCell;

	void Start()
	{
		_gameController = GameController.Instance;
		_aStar = gameObject.AddComponent<AStar>();

		// TODO: Change to prefab.
		_targetCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		_targetCube.GetComponent<Renderer>().material.SetColor("_Color", Color.blue);
		_targetCube.transform.localScale *= 10;

		CalculatePath(NewTargetCellPosition());
	}

	/// <summary>
	/// Returns acceleration towards a goal for one boid.
	/// </summary>
	/// <returns>A threedimensional vector with acceleration values.</returns>
	public Vector3 Explore(Predator predator)
	{
		var currentPosition = transform.position;
		var currentCellPosition = _gameController.Graph.WorldToCellPosition(currentPosition);
		var currentCell = _gameController.Graph.GetCell(currentCellPosition);
		currentCell.Visited = true;

		// At the target position.
		if (currentCell == _targetCell)
		{
			CalculatePath(NewTargetCellPosition());
		}

		// At the current target's cell and there are still nodes left in the path.
		if (currentCellPosition == _currentTargetCell && _currentPath.Any())
		{
			_currentTargetCell = _currentPath.Pop().Position;
		}

		// Arrive if last node.
		if (_currentPath != null && _currentPath.Count <= 1)
		{
			return Behaviours.Arrive(
				currentPosition,
				_gameController.MaxSteerForce,
				_gameController.MaxSpeed,
				predator.Velocity,
				_gameController.Graph.CellToWorldPosition(_currentTargetCell));
		}

		return Behaviours.Reach(
			currentPosition,
			_gameController.Graph.CellToWorldPosition(_currentTargetCell),
			_gameController.MaxSteerForce);
	}

	public Vector3Int NewTargetCellPosition()
	{
		_gameController.Graph.UpdateUnvisitedCells();

		Vector3Int targetCellPosition = _gameController.Graph.UnvisitedPositions.First.Value;
		TargetPosition = _gameController.Graph.CellToWorldPosition(targetCellPosition);
		_targetCell = _gameController.Graph.GetCell(targetCellPosition);
		MoveCubeToTarget();
		return targetCellPosition;
	}

	private void MoveCubeToTarget()
	{
		_targetCube.transform.position = TargetPosition;
	}

	public void CalculatePath(Vector3Int targetCellPosition)
	{
		var path = _aStar.CalculatePath(_gameController.Graph.Matrix,
			_gameController.Graph.WorldToCellPosition(transform.position), targetCellPosition);

		if (!path.Any())
		{
			return;
		}

		_currentPath = new Stack<Node>(path.Reverse());
		_currentTargetCell = _currentPath.Pop().Position;
	}
}