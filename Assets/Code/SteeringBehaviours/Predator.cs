using UnityEngine;

public class Predator : MonoBehaviour
{
	[SerializeField] private TextFollower _textFollowerPrefab;

	private GameController _gameController;

	public Cell CurrentCell { get; private set; }
	public Vector3 Velocity { get; private set; }
	public bool Resting { get; private set; }
	public float Energy { get; set; }
	public float DistToClosestTarget { get; set; }
	public ExploreBehaviour ExploreBehaviour { get; private set; }
	public StateMachine StateMachine { get; private set; }
	public FuzzyLogic FuzzyLogic { get; private set; }
	[HideInInspector] public Boid TargetBoid;
	public double AttackDistance { get; private set; }
	public double ExploreDistance { get; private set; }

	private Vector3 _acceleration;
	private Transform _transform;
	private TextFollower _textFollower;

	private void Start()
	{
		_gameController = GameController.Instance;
		ExploreBehaviour = gameObject.GetComponent<ExploreBehaviour>();
		Resting = false;

		_acceleration = new Vector3();
		Velocity = new Vector3();
		_transform = transform;

		_textFollower = Instantiate(_textFollowerPrefab);
		_textFollower.Target = _transform;
		StateMachine = new StateMachine(this) {CurrentState = RestState.Instance};
		FuzzyLogic = new FuzzyLogic();

		AttackDistance = 0d;
		ExploreDistance = 0d;
		TargetBoid = _gameController.RandomBoid();
	}

	public void Update()
	{
		AttackDistance = Vector3.Distance(_transform.position, TargetBoid.transform.position);
		ExploreDistance = AStar.CalculateHeuristic(
			                  _gameController.Graph.WorldToCellPosition(_transform.position),
			                  _gameController.Graph.WorldToCellPosition(ExploreBehaviour.TargetPosition),
			                  _gameController.AStarHeuristic) * _gameController.Graph.CellSize;

		CurrentCell = _gameController.Graph.GetCell(_transform.position);
		StateMachine.Update();
		_textFollower.Text = "State: " + StateMachine.CurrentState.GetType().Name + "\nEnergy: " + (int) Energy +
		                     "\nAttackDist: " +
		                     (int) AttackDistance + "\nExploreDist: " + (int) ExploreDistance;
		Move();
		SetHeading();
	}

	private void Move()
	{
		Velocity = Vector3.ClampMagnitude(Velocity + _acceleration, _gameController.MaxSpeed);
		_transform.position += Velocity;
		_acceleration = Vector3.zero;
	}

	private void SetHeading()
	{
		if (Velocity != Vector3.zero)
		{
			_transform.forward = Velocity;
		}
	}

	public void Attack()
	{
		_acceleration += Behaviours.Reach(_transform.position, TargetBoid.transform.position, 0.001f);
	}

	public void Rest()
	{
		_acceleration += Behaviours.Arrive(_transform.position, _gameController.MaxSteerForce, _gameController.MaxSpeed,
			Velocity, GameController.Instance.Graph.CellToWorldPosition(Vector3Int.zero));
	}

	public void Explore()
	{
		_acceleration += ExploreBehaviour.Explore(this);
	}
}