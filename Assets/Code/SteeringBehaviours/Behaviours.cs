﻿using System.Collections.Generic;
using UnityEngine;

public static class Behaviours
{
	public static Vector3 Avoid(Vector3 target, Vector3 currentPosition)
	{
		var steer = new Vector3(currentPosition.x, currentPosition.y, currentPosition.z);
		steer -= target;
		steer *= 1 / (currentPosition - target).sqrMagnitude;

		return steer;
	}

	public static Vector3 Repulse(Vector3 target, Vector3 currentPosition, float maxDistance)
	{
		var distance = Vector3.Distance(currentPosition, target);

		if (distance < maxDistance)
		{
			var steer = currentPosition - target;
			steer *= .5f / distance;
			return steer;
		}

		return Vector3.zero;
	}

	public static Vector3 Reach(Vector3 from, Vector3 target, float amount)
	{
		var steer = target - from;
		steer *= amount;

		return steer;
	}

	public static Vector3 Alignment(IEnumerable<Boid> boids, float maxSteerForce)
	{
		var count = 0;
		var velSum = new Vector3();

		foreach (var boid in boids)
		{
			if (Random.value > 0.6f)
			{
				continue;
			}

			velSum += boid.Velocity;
			count++;
		}

		if (count > 0)
		{
			velSum /= count;

			var l = velSum.magnitude;

			if (l > maxSteerForce)
			{
				velSum /= l / maxSteerForce;
			}
		}

		return velSum;
	}

	public static Vector3 Cohesion(IEnumerable<Boid> boids, Vector3 currentLocation, float maxSteerForce)
	{
		var count = 0;
		var posSum = new Vector3();

		foreach (var boid in boids)
		{
			if (Random.value > 0.6f)
			{
				continue;
			}

			posSum += boid.transform.position;
			count++;
		}

		if (count > 0)
		{
			posSum /= count;
		}

		var steer = posSum - currentLocation;
		var l = steer.magnitude;

		if (l > maxSteerForce)
		{
			steer /= l / maxSteerForce;
		}

		return steer;
	}

	public static Vector3 Separation(IEnumerable<Boid> boids, Vector3 currentLocation)
	{
		var posSum = new Vector3();

		foreach (var boid in boids)
		{
			if (Random.value > 0.6f) continue;

			var distance = Vector3.Distance(boid.transform.position, currentLocation);
			if (distance > 0)
			{
				var repulse = currentLocation - boid.transform.position;
				repulse.Normalize();
				repulse /= distance;
				posSum += repulse;
			}
		}

		return posSum;
	}

	public static Vector3 Arrive(Vector3 position, float maxSteerForce, float maxSpeed, Vector3 velocity, Vector3 target)
	{
		var toTarget = target - position;
		var distance = toTarget.magnitude;

		if (distance > 0)
		{
			var decelerationTweaker = 20f;
			var speed = distance / decelerationTweaker;
			speed = Mathf.Min(speed, maxSpeed);
			var desiredVelocity = toTarget * speed / distance;

			return (desiredVelocity - velocity) * maxSteerForce;
		}

		return Vector3.zero;
	}
}