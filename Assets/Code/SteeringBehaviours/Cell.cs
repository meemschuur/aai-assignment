﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Cell
{
	public bool Visited { get; set; }

	public bool Occupied
	{
		get
		{
			if (BoidsInside == null)
			{
				return false;
			}

			return BoidsInside.Any();
		}
	}

	public LinkedList<Boid> BoidsInside { get; private set; }

	public void AddBoid(Boid boid)
	{
		if (BoidsInside == null)
		{
			BoidsInside = new LinkedList<Boid>();
		}

		BoidsInside.AddFirst(boid);
	}

	public void RemoveBoid(Boid boid)
	{
		if (BoidsInside == null)
		{
			return;
		}

		BoidsInside.Remove(boid);
	}
}