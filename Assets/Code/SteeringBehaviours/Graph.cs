using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Graph : MonoBehaviour
{
	public int CellSize = 10;
	public LinkedList<Vector3Int> UnvisitedPositions;

	private GameController _gameController;

	public Cell[,,] Matrix { get; private set; }
	private Vector3Int _matrixBounds;

	private void Start()
	{
		_gameController = GameController.Instance;

		Vector3Int cellAmount = new Vector3Int(
			_gameController.WorldSize.x * 2 / CellSize,
			_gameController.WorldSize.y * 2 / CellSize,
			_gameController.WorldSize.z * 2 / CellSize);

		Matrix = new Cell[cellAmount.z, cellAmount.y, cellAmount.x];

		var tempList = new LinkedList<Vector3Int>();
		for (int z = 0; z < cellAmount.z; z++)
		{
			for (int y = 0; y < cellAmount.y; y++)
			{
				for (int x = 0; x < cellAmount.x; x++)
				{
					Matrix[z, y, x] = new Cell();
					tempList.AddFirst(new Vector3Int(x, y, z));
				}
			}
		}

		// Subtract one to calculate bounds, because arrays are zero-indexed.
		_matrixBounds = cellAmount - Vector3Int.one;

		var tempEnumerable = tempList.OrderBy(x => Random.value);

		UnvisitedPositions = new LinkedList<Vector3Int>(tempEnumerable);
	}

	public void UpdateCell(Boid boid)
	{
		var previousCell = boid.CurrentCell;
		var newCell = GetCell(boid.transform.position);

		if (previousCell == newCell)
		{
			return;
		}

		if (previousCell != null)
		{
			boid.CurrentCell.RemoveBoid(boid);
		}

		newCell.AddBoid(boid);
		boid.CurrentCell = newCell;
	}

	public Vector3Int GetRandomCellPosition()
	{
		return new Vector3Int(Random.Range(0, _matrixBounds.x), Random.Range(0, _matrixBounds.y),
			Random.Range(0, _matrixBounds.z));
	}

	public Cell GetCell(Vector3 worldPosition)
	{
		var cellPosition = WorldToCellPosition(worldPosition);

		return GetCell(cellPosition);
	}

	public Cell GetCell(Vector3Int cellPosition)
	{
		cellPosition.Clamp(Vector3Int.zero, _matrixBounds);
		return Matrix[cellPosition.z, cellPosition.y, cellPosition.x];
	}

	/// <summary>
	/// Converts world position to cell position (hashing method).
	/// </summary>
	/// <param name="worldPosition">The position to convert.</param>
	/// <returns></returns>
	public Vector3Int WorldToCellPosition(Vector3 worldPosition)
	{
		Vector3Int cellPosition = new Vector3Int(
			Mathf.CeilToInt(worldPosition.x + _gameController.WorldSize.x) / CellSize,
			Mathf.CeilToInt(worldPosition.y + _gameController.WorldSize.y) / CellSize,
			Mathf.CeilToInt(worldPosition.z + _gameController.WorldSize.z) / CellSize);

		cellPosition.Clamp(Vector3Int.zero, _matrixBounds);
		return cellPosition;
	}

	public Vector3 CellToWorldPosition(Vector3Int cellPosition)
	{
		if (cellPosition.x > _matrixBounds.x ||
		    cellPosition.y > _matrixBounds.y ||
		    cellPosition.z > _matrixBounds.z)
		{
			throw new ArgumentException("Cell position should be inside bounds");
		}

		return new Vector3(
			cellPosition.x * CellSize - _gameController.WorldSize.x + CellSize / 2,
			cellPosition.y * CellSize - _gameController.WorldSize.y + CellSize / 2,
			cellPosition.z * CellSize - _gameController.WorldSize.z + CellSize / 2);
	}

	public void UpdateUnvisitedCells()
	{
		// Removing until we find an unvisited cell.
		while (GetCell(UnvisitedPositions.First.Value).Visited)
		{
			UnvisitedPositions.RemoveFirst();
		}
	}

	public IEnumerable<Boid> GetClosestBoids(Vector3 worldPosition, int range)
	{
		var cellPosition = WorldToCellPosition(worldPosition);
		cellPosition.Clamp(Vector3Int.zero, _matrixBounds);

		return GetClosestBoids(cellPosition, range);
	}

	private IEnumerable<Boid> GetClosestBoids(Vector3Int cellPosition, int range)
	{
		List<Boid> boidCells = new List<Boid>();

		for (int i = -range; i <= range; i++)
		{
			for (int j = -range; j <= range; j++)
			{
				for (int k = -range; k <= range; k++)
				{
					int x = cellPosition.x + i;
					if (x < 0 || x >= _matrixBounds.x)
					{
						continue;
					}

					int y = cellPosition.y + j;
					if (y < 0 || y >= _matrixBounds.y)
					{
						continue;
					}

					int z = cellPosition.z + k;
					if (z < 0 || z >= _matrixBounds.z)
					{
						continue;
					}

					var currentCell = Matrix[z, y, x];
					if (currentCell != null && currentCell.BoidsInside != null)
					{
						boidCells.AddRange(currentCell.BoidsInside);
					}
				}
			}
		}

		return boidCells;
	}
}