﻿public interface IState
{
	void Enter(Predator predator);
	void Execute(Predator predator);
	void Exit(Predator predator);
}