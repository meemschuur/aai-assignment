﻿public class ExploreState : IState
{
	private static ExploreState _instance;

	public static ExploreState Instance
	{
		get { return _instance ?? (_instance = new ExploreState()); }
	}

	public void Enter(Predator predator)
	{
		predator.ExploreBehaviour.CalculatePath(predator.ExploreBehaviour.NewTargetCellPosition());
	}

	public void Execute(Predator predator)
	{
		predator.Explore();
		predator.Energy -= 0.01f;
	}

	public void Exit(Predator predator)
	{
	}
}