﻿using System;

public class StateMachine
{
	private Predator _predator;

	public IState CurrentState { get; set; }
	public IState PreviousState { get; set; }
	public IState GlobalState { get; set; }

	public StateMachine(Predator predator)
	{
		_predator = predator;
		CurrentState = null;
		PreviousState = null;
		GlobalState = null;
	}

	public void Update()
	{
		if (GlobalState != null)
		{
			GlobalState.Execute(_predator);
		}

		if (CurrentState != null)
		{
			var mostDesiredState = _predator.FuzzyLogic.GetMostDesiredState(_predator, FuzzyModule.DefuzzifyType.Centroid);
			if (mostDesiredState != CurrentState)
			{
				ChangeState(mostDesiredState);
			}

			CurrentState.Execute(_predator);
		}
	}

	public void ChangeState(IState newState)
	{
		if (newState == null)
		{
			throw new Exception("newState is null");
		}

		PreviousState = CurrentState;
		CurrentState.Exit(_predator);
		CurrentState = newState;
		CurrentState.Enter(_predator);
	}

	public void RevertToPreviousState()
	{
		ChangeState(PreviousState);
	}

	public bool IsInState(IState state)
	{
		return state == CurrentState;
	}
}