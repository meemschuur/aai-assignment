﻿public class AttackState : IState
{
	private static AttackState _instance;

	public static AttackState Instance
	{
		get { return _instance ?? (_instance = new AttackState()); }
	}

	public void Enter(Predator predator)
	{
		predator.TargetBoid = GameController.Instance.RandomBoid();
	}

	public void Execute(Predator predator)
	{
		predator.Attack();
		predator.Energy -= 0.02f;
		if (predator.CurrentCell == predator.TargetBoid.CurrentCell)
		{
			Enter(predator);
		}
	}

	public void Exit(Predator predator)
	{
	}
}