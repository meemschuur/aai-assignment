﻿using UnityEngine;

public class RestState : IState
{
	private static RestState _instance;

	public static RestState Instance
	{
		get { return _instance ?? (_instance = new RestState()); }
	}

	public void Enter(Predator predator)
	{
	}

	public void Execute(Predator predator)
	{
		predator.Rest();

		if (GameController.Instance.Graph.GetCell(Vector3Int.zero) == predator.CurrentCell)
		{
			predator.Energy = 20;
		}
	}

	public void Exit(Predator predator)
	{
	}
}