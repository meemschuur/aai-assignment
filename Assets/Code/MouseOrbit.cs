﻿using UnityEngine;

public class MouseOrbit : MonoBehaviour
{
	[SerializeField] private Transform _target;
	[SerializeField] private float _xSpeed = 250;
	[SerializeField] private float _ySpeed = 250;
	[SerializeField] private float _zoomSpeed = 30;

	private Vector3 _initialPosition;
	private float _distance;

	private float _x;
	private float _y;

	private void Start()
	{
		_initialPosition = transform.position;
		_distance = _initialPosition.z;
		_initialPosition.z = 0;
		Vector3 angles = transform.eulerAngles;
		_x = angles.y;
		_y = angles.x;
	}

	private void LateUpdate()
	{
		if (!_target)
		{
			return;
		}

		_distance = _distance - Input.GetAxis("Mouse ScrollWheel") * _zoomSpeed;

		if (Input.GetKey(KeyCode.LeftControl))
		{
			_x += Input.GetAxis("Mouse X") * _xSpeed;
			_y -= Input.GetAxis("Mouse Y") * _ySpeed;
		}

		Quaternion rotation = Quaternion.Euler(_y, _x, 0);

		Vector3 negDistance = new Vector3(0.0f, 0.0f, -_distance);
		Vector3 position = rotation * negDistance + _target.position + _initialPosition;

		transform.rotation = rotation;
		transform.position = position;
	}
}