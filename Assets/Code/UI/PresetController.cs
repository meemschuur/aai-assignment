﻿using UnityEngine;
using UnityEngine.UI;

public class PresetController : MonoBehaviour
{
	[SerializeField] private Option[] _options;

	[SerializeField] private Slider _allignmentSlider;
	[SerializeField] private Slider _cohesionSlider;
	[SerializeField] private Slider _separationSlider;

	private Dropdown _dropdown;

	void Start()
	{
		_dropdown = GetComponent<Dropdown>();
		foreach (var option in _options)
		{
			_dropdown.options.Add(new Dropdown.OptionData(option.Name));
		}

		_dropdown.RefreshShownValue();
		OnValueChanged();
	}

	public void OnValueChanged()
	{
		var i = _dropdown.value;
		_allignmentSlider.value = _options[i].Allignment;
		_cohesionSlider.value = _options[i].Cohesion;
		_separationSlider.value = _options[i].Separation;
	}
}