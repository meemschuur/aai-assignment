﻿using UnityEngine;
using UnityEngine.UI;

public class TextFollower : MonoBehaviour
{
	public Transform Target;
	[SerializeField] private Text _text;

	public string Text
	{
		get { return _text.text; }
		set { _text.text = value; }
	}

	private Camera _camera;

	private void Start()
	{
		_camera = Camera.main;
		transform.SetParent(GameObject.FindGameObjectWithTag("Canvas").transform, false);
	}

	private void Update()
	{
		if (!_camera || !Target)
		{
			return;
		}

		transform.position = _camera.WorldToScreenPoint(Target.position);
	}
}