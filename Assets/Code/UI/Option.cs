﻿using System;

[Serializable]
public class Option
{
	public string Name;
	public float Allignment;
	public float Cohesion;
	public float Separation;
}