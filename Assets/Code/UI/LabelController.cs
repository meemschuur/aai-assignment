﻿using UnityEngine;
using UnityEngine.UI;

public class LabelController : MonoBehaviour
{
	[SerializeField] private Text _text;

	public void OnChange(float value)
	{
		_text.text = string.Format("{0:F}", value);
	}
}