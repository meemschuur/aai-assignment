﻿using System.Collections.Generic;
using UnityEngine;

public class DrawGizmoLines : MonoBehaviour
{
	public static DrawGizmoLines Instance;

	[SerializeField] private Color _mainColor = new Color(0f, 1f, 0f, 1f);

	public IEnumerable<Node> Path;

	private int _stepSize;
	private Vector3Int _start;

	private Material _lineMaterial;
	private GameController _gameController;

	private void Awake()
	{
		// Remove GameObject if the Singleton already exists.
		if (Instance != null)
		{
			Destroy(gameObject);
			return;
		}

		Instance = this;
	}

	private void Start()
	{
		_gameController = GameController.Instance;
	}

	private void CreateLineMaterial()
	{
		if (_lineMaterial)
		{
			return;
		}

		// Unity has a built-in shader that is useful for drawing simple colored things.
		var shader = Shader.Find("Hidden/Internal-Colored");
		_lineMaterial = new Material(shader);
		_lineMaterial.hideFlags = HideFlags.HideAndDontSave;

		// Turn on alpha blending.
		_lineMaterial.SetInt("_SrcBlend", (int) UnityEngine.Rendering.BlendMode.SrcAlpha);
		_lineMaterial.SetInt("_DstBlend", (int) UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

		// Turn backface culling off.
		_lineMaterial.SetInt("_Cull", (int) UnityEngine.Rendering.CullMode.Off);

		// Turn off depth writes.
		_lineMaterial.SetInt("_ZWrite", 0);
	}

	private void OnPostRender()
	{
		_stepSize = _gameController.Graph.CellSize;

		if (_stepSize <= 0)
		{
			return;
		}

		Vector3Int gridSize = _gameController.WorldSize * 2;
		_start = _gameController.WorldSize * -1;

		CreateLineMaterial();

		// Set the current material.
		_lineMaterial.SetPass(0);

		if (GameController.Instance.ShouldDrawGrid)
		{
			GL.Begin(GL.LINES);
			GL.Color(_mainColor);

			DrawGrid(0, gridSize.y, gridSize.z);
			DrawGrid(gridSize.x, 0, gridSize.z);
			DrawGrid(gridSize.x, gridSize.y, 0);

			GL.End();
		}

		if (GameController.Instance.ShouldDrawPath)
		{
			GL.Begin(GL.LINE_STRIP);
			GL.Color(Color.red);
			DrawPath(Path);
			GL.End();
		}
	}

	private void DrawPath(IEnumerable<Node> path)
	{
		if (path == null)
		{
			return;
		}

		foreach (var node in path)
		{
			var worldPosition = _gameController.Graph.CellToWorldPosition(node.Position);
			GL.Vertex3(worldPosition.x, worldPosition.y, worldPosition.z);
		}
	}

	private void DrawGrid(int x, int y, int z)
	{
		// Layers.
		for (float j = 0; j <= y; j += _stepSize)
		{
			// X axis lines.
			for (float i = 0; i <= z; i += _stepSize)
			{
				GL.Vertex3(_start.x, _start.y + j, _start.z + i);
				GL.Vertex3(_start.x + x, _start.y + j, _start.z + i);
			}

			// Z axis lines.
			for (float i = 0; i <= x; i += _stepSize)
			{
				GL.Vertex3(_start.x + i, _start.y + j, _start.z);
				GL.Vertex3(_start.x + i, _start.y + j, _start.z + z);
			}
		}

		// Y axis lines.
		for (float i = 0; i <= z; i += _stepSize)
		{
			for (float k = 0; k <= x; k += _stepSize)
			{
				GL.Vertex3(_start.x + k, _start.y, _start.z + i);
				GL.Vertex3(_start.x + k, _start.y + y, _start.z + i);
			}
		}
	}
}