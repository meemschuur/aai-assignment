﻿using UnityEngine;

public class Node
{
	/// <summary>
	/// Distance from the starting node.
	/// </summary>
	public int From;

	/// <summary>
	/// Distance to the destination node.
	/// </summary>
	public int To;

	/// <summary>
	/// The sum of From and To.
	/// </summary>
	public int Sum;

	/// <summary>
	/// The previous node in the node's path.
	/// </summary>
	public Node Parent;

	public Vector3Int Position;
}