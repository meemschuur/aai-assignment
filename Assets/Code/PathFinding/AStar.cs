﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum Heuristic
{
	Manhattan,
	Euclidean
}

/// <summary>
/// 2D A* from: https://stackoverflow.com/a/38034226/2771689.
/// </summary>
public class AStar : MonoBehaviour
{
	[SerializeField] private bool _shouldPrintPath;

	/// <summary>
	/// Add these values to current node's x, y and z values to get its left/right/down/up/front/back neighbours.
	/// </summary>
	private readonly List<Vector3Int> _neighbours = new List<Vector3Int>
	{
		new Vector3Int(-1, 0, 0),
		new Vector3Int(1, 0, 0),
		new Vector3Int(0, -1, 0),
		new Vector3Int(0, 1, 0),
		new Vector3Int(0, 0, -1),
		new Vector3Int(0, 0, 1)
	};

	public LinkedList<Node> CalculatePath(Cell[,,] matrix, Vector3Int from, Vector3Int to)
	{
		Node endNode = Run(matrix, from, to);
		var path = new LinkedList<Node>();
		if (endNode == null)
		{
			Debug.LogWarning("No path is possible to the target node.");
			DrawGizmoLines.Instance.Path = path;
			return path;
		}

		// Looping through the parent nodes until we get to the start node. This was a stack at first, but it's nice
		// to have a LinkedList to be able to loop over the list multiple times.
		while (endNode.Position.x != from.x || endNode.Position.y != from.y || endNode.Position.z != from.z)
		{
			path.AddFirst(endNode);
			endNode = endNode.Parent;
		}

		// Adding the start node.
		path.AddFirst(endNode);
		DrawGizmoLines.Instance.Path = path;

		if (_shouldPrintPath)
		{
			PrintPath(from, to, path);
		}

		return path;
	}

	private void PrintPath(Vector3Int from, Vector3Int to, IEnumerable<Node> path)
	{
		StringBuilder sb = new StringBuilder();
		sb.AppendLine("The shortest path from " + from + " to " + to + " is:");
		foreach (var node in path)
		{
			sb.AppendLine(node.Position.ToString());
		}

		print(sb.ToString());
	}

	private Node Run(Cell[,,] matrix, Vector3Int from, Vector3Int to)
	{
		Dictionary<Vector3Int, Node> open = new Dictionary<Vector3Int, Node>();
		Dictionary<Vector3Int, Node> closed = new Dictionary<Vector3Int, Node>();

		Node startNode = new Node();
		startNode.Position = from;
		open.Add(from, startNode);

		// If from or target is occupied, don't execute the algorithm.
		if (matrix[from.z, from.y, from.x].Occupied || matrix[to.z, to.y, to.z].Occupied)
		{
			return null;
		}

		int maxZ = matrix.GetLength(0);
		if (maxZ == 0)
		{
			return null;
		}

		int maxY = matrix.GetLength(1);
		int maxX = matrix.GetLength(2);

		while (true)
		{
			if (open.Count == 0)
			{
				return null;
			}

			KeyValuePair<Vector3Int, Node> current = SmallestOpen(open);
			if (current.Value.Position.x == to.x && current.Value.Position.y == to.y && current.Value.Position.z == to.z)
			{
				return current.Value;
			}

			open.Remove(current.Key);
			closed.Add(current.Key, current.Value);

			foreach (Vector3Int neighbour in _neighbours)
			{
				Vector3Int neighbourLocation = current.Value.Position + neighbour;

				// If outside bounds, has an obstacle, or isn't an open node ...
				if (neighbourLocation.x < 0 || neighbourLocation.y < 0 || neighbourLocation.z < 0
				    || neighbourLocation.x >= maxX || neighbourLocation.y >= maxY || neighbourLocation.z >= maxZ
				    || matrix[neighbourLocation.z, neighbourLocation.y, neighbourLocation.x].Occupied
				    || closed.ContainsKey(neighbourLocation))
				{
					// ... move to next neighbour.
					continue;
				}

				Node currentNeighbour;
				if (open.ContainsKey(neighbourLocation))
				{
					currentNeighbour = open[neighbourLocation];
					int fromStart = CalculateHeuristic(from, neighbourLocation, GameController.Instance.AStarHeuristic);
					if (fromStart < currentNeighbour.From)
					{
						currentNeighbour.From = fromStart;
						currentNeighbour.Sum = currentNeighbour.From + currentNeighbour.To;
						currentNeighbour.Parent = current.Value;
					}
				}
				else
				{
					currentNeighbour = new Node
					{
						Position = neighbourLocation,
						From = CalculateHeuristic(from, neighbourLocation, GameController.Instance.AStarHeuristic),
						To = CalculateHeuristic(to, neighbourLocation, GameController.Instance.AStarHeuristic),
						Parent = current.Value
					};
					currentNeighbour.Sum = currentNeighbour.From + currentNeighbour.To;
					open.Add(neighbourLocation, currentNeighbour);
				}
			}
		}
	}


	public static int CalculateHeuristic(Vector3Int start, Vector3Int target, Heuristic heuristic)
	{
		switch (heuristic)
		{
			case Heuristic.Manhattan:
				return ManhattanDistance(start, target);
			case Heuristic.Euclidean:
				return EuclideanDistance(start, target);
		}

		// If an unknown heuristic is specified, just use Dijkstra instead.
		return 0;
	}

	private static int ManhattanDistance(Vector3Int start, Vector3Int target)
	{
		return Math.Abs(target.x - start.x) + Math.Abs(target.y - start.y) + Math.Abs(target.z - start.z);
	}

	private static int EuclideanDistance(Vector3Int start, Vector3Int target)
	{
		return (int) Vector3Int.Distance(start, target);
	}

	private KeyValuePair<Vector3Int, Node> SmallestOpen(Dictionary<Vector3Int, Node> open)
	{
		KeyValuePair<Vector3Int, Node> smallest = open.ElementAt(0);
		foreach (KeyValuePair<Vector3Int, Node> item in open)
		{
			if (item.Value.Sum < smallest.Value.Sum)
			{
				smallest = item;
			}
			else if (item.Value.Sum == smallest.Value.Sum && item.Value.To < smallest.Value.To)
			{
				smallest = item;
			}
		}

		return smallest;
	}
}