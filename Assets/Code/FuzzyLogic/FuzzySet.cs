﻿using System;

public abstract class FuzzySet
{
	private readonly double _representativeValue;
	private double _dom;

	protected FuzzySet(double repVal)
	{
		_representativeValue = repVal;
		_dom = 0d;
	}

	public double DOM
	{
		get { return _dom; }
		set
		{
			if (value <= 1 && value >= 0)
			{
				_dom = value;
			}
			else
			{
				throw new Exception("Invalid value");
			}
		}
	}

	public void ClearDOM()
	{
		_dom = 0d;
	}

	public abstract double CalculateDOM(double val);

	public double GetRepresentativeValue()
	{
		return _representativeValue;
	}

	public void ORwithDOM(double val)
	{
		if (val > _dom)
		{
			_dom = val;
		}
	}
}