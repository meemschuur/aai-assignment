﻿public abstract class FuzzyTerm
{
	public abstract double DOM { get; }

	public abstract void ClearDOM();

	public abstract void ORwithDOM(double val);
}