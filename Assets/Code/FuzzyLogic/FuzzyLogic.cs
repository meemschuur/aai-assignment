﻿using System.Collections.Generic;

public class FuzzyLogic
{
	private readonly List<IRuleSet> _ruleSets;

	public FuzzyLogic()
	{
		_ruleSets = new List<IRuleSet>
		{
			new AttackRuleSet(),
			new RestRuleSet(),
			new ExploreRuleSet()
		};
	}

	public IState GetMostDesiredState(Predator predator, FuzzyModule.DefuzzifyType type)
	{
		double distanceToTarget;
		IState curHighestDesirableInstance = predator.StateMachine.CurrentState;
		var highestScore = _ruleSets.Find(x => x.CorrespondingStateType == curHighestDesirableInstance)
			.GetDesirability(predator.DistToClosestTarget, predator.Energy, type);

		if (curHighestDesirableInstance == AttackState.Instance || curHighestDesirableInstance == RestState.Instance)
		{
			distanceToTarget = predator.AttackDistance;
		}
		else
		{
			distanceToTarget = predator.ExploreDistance;
		}

		foreach (var ruleSet in _ruleSets)
		{
			var curScore = ruleSet.GetDesirability(distanceToTarget, predator.Energy, type);
			if (curScore <= highestScore) continue;
			curHighestDesirableInstance = ruleSet.CorrespondingStateType;
			highestScore = curScore;
		}

		return curHighestDesirableInstance;
	}
}