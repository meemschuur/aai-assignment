﻿using System.Collections.Generic;

public class FuzzyAND : FuzzyTerm
{
	private readonly List<FuzzyTerm> _terms;

	public FuzzyAND(FuzzyTerm op1, FuzzyTerm op2)
	{
		_terms = new List<FuzzyTerm> {op1, op2};
	}

	public FuzzyAND(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3)
	{
		_terms = new List<FuzzyTerm> {op1, op2, op3};
	}

	public FuzzyAND(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3, FuzzyTerm op4)
	{
		_terms = new List<FuzzyTerm> {op1, op2, op3, op4};
	}

	public override double DOM
	{
		get
		{
			var smallest = double.MaxValue;

			foreach (var term in _terms)
			{
				if (term.DOM < smallest)
				{
					smallest = term.DOM;
				}
			}

			return smallest;
		}
	}

	public override void ClearDOM()
	{
		foreach (var term in _terms)
		{
			term.ClearDOM();
		}
	}

	public override void ORwithDOM(double val)
	{
		foreach (var term in _terms)
		{
			term.ORwithDOM(val);
		}
	}
}