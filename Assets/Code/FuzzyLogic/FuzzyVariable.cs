﻿using System;
using System.Collections.Generic;

public class FuzzyVariable
{
	private readonly Dictionary<string, FuzzySet> _memberSets;
	private double _minRange;
	private double _maxRange;

	public FuzzyVariable()
	{
		_memberSets = new Dictionary<string, FuzzySet>();
	}

	private void AdjustRangeToFit(double min, double max)
	{
		if (min < _minRange)
		{
			_minRange = min;
		}

		if (max > _maxRange)
		{
			_maxRange = max;
		}
	}

	public FzSet AddTriangularSet(string name, double minBound, double peak, double maxBound)
	{
		_memberSets[name] = new FuzzySet_Triangle(peak, peak - minBound, maxBound - peak);
		AdjustRangeToFit(minBound, maxBound);
		return new FzSet(_memberSets[name]);
	}

	public FzSet AddRightShoulderSet(string name, double minBound, double peak, double maxBound)
	{
		_memberSets[name] = new FuzzySet_RightShoulder(peak, peak - minBound, maxBound - peak);
		AdjustRangeToFit(minBound, maxBound);
		return new FzSet(_memberSets[name]);
	}

	public FzSet AddLeftShoulderSet(string name, double minBound, double peak, double maxBound)
	{
		_memberSets[name] = new FuzzySet_LeftShoulder(peak, peak - minBound, maxBound - peak);
		AdjustRangeToFit(minBound, maxBound);
		return new FzSet(_memberSets[name]);
	}

	public void Fuzzify(double val)
	{
		if (val <= _minRange && val >= _maxRange)
		{
			throw new Exception("Value out of range.");
		}

		foreach (var member in _memberSets)
		{
			var calculatedDOM = member.Value.CalculateDOM(val);
			member.Value.DOM = calculatedDOM;
		}
	}

	public double DefuzzifyMaxAv()
	{
		var bottom = 0d;
		var top = 0d;

		foreach (var member in _memberSets)
		{
			bottom += member.Value.DOM;
			top += member.Value.GetRepresentativeValue() * member.Value.DOM;
		}

		if (bottom == 0d)
		{
			return 0d;
		}

		return top / bottom;
	}

	public double DefuzzifyCentroid(int numSamples)
	{
		var stepSize = (_maxRange - _minRange) / numSamples;
		var totalArea = 0d;
		var sumOfMoments = 0d;

		for (int samp = 1; samp <= numSamples; ++samp)
		{
			foreach (var member in _memberSets)
			{
				var contribution = Math.Min(member.Value.CalculateDOM(_minRange + samp * stepSize), member.Value.DOM);
				totalArea += contribution;
				sumOfMoments += (_minRange + samp * stepSize) * contribution;
			}
		}

		if (totalArea == 0d)
		{
			return 0d;
		}

		return sumOfMoments / totalArea;
	}
}