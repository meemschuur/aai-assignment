﻿public interface IRuleSet
{
	IState CorrespondingStateType { get; }
	FuzzyModule FM { get; }
	void CreateRuleSet();
	double GetDesirability(double distToTarget, double energyLeft, FuzzyModule.DefuzzifyType defuzzifyType);
}