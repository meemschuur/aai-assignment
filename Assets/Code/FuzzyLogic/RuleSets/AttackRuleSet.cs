﻿public class AttackRuleSet : IRuleSet
	{
		public AttackRuleSet()
		{
			CreateRuleSet();
		}

		public IState CorrespondingStateType
		{
			get { return AttackState.Instance; }
		}

		public FuzzyModule FM { get; private set; }

		public void CreateRuleSet()
		{
			FM = new FuzzyModule();
			// Antecedant 1 
			var distToTarget = FM.CreateFLV("DistToTarget");
			var targetClose = distToTarget.AddLeftShoulderSet("Target_Close", 0, 25, 150);
			var targetMedium = distToTarget.AddTriangularSet("Target_Medium", 25, 50, 300);
			var targetFar = distToTarget.AddRightShoulderSet("Target_Far", 150, 400, 1000);

			// Antecedant 2
			var energyStatus = FM.CreateFLV("EnergyStatus");
			var energyLots = energyStatus.AddRightShoulderSet("Energy_Lots", 10, 15, 20);
			var energyOkay = energyStatus.AddTriangularSet("Energy_Okay", 2, 10, 15);
			var energyLow = energyStatus.AddLeftShoulderSet("Energy_Low", 0, 1, 3);

			// Consequent
			var desirability = FM.CreateFLV("Desirability");
			var veryDesirable = desirability.AddRightShoulderSet("Very_Desirable", 50, 75, 100);
			var desirable = desirability.AddTriangularSet("Desirable", 25, 50, 75);
			var undesirable = desirability.AddLeftShoulderSet("Undesirable", 0, 25, 50);

			FM.AddRule(new FuzzyAND(targetClose, energyLow), undesirable);
			FM.AddRule(new FuzzyAND(targetClose, energyOkay), desirable);
			FM.AddRule(new FuzzyAND(targetClose, energyLots), desirable);

			FM.AddRule(new FuzzyAND(targetMedium, energyLow), undesirable);
			FM.AddRule(new FuzzyAND(targetMedium, energyOkay), veryDesirable);
			FM.AddRule(new FuzzyAND(targetMedium, energyLots), veryDesirable);

			FM.AddRule(new FuzzyAND(targetFar, energyLow), desirable);
			FM.AddRule(new FuzzyAND(targetFar, energyOkay), veryDesirable);
			FM.AddRule(new FuzzyAND(targetFar, energyLots), veryDesirable);
		}

		public double GetDesirability(double distToTarget, double energyLeft, FuzzyModule.DefuzzifyType defuzzifyType)
		{
			FM.Fuzzify("DistToTarget", distToTarget);
			FM.Fuzzify("EnergyStatus", energyLeft);

			return FM.Defuzzify("Desirability", defuzzifyType);
		}
	}
