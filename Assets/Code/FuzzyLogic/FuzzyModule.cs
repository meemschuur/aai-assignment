﻿using System;
using System.Collections.Generic;

public class FuzzyModule
{
	public enum DefuzzifyType
	{
		MaxAv,
		Centroid
	}

	private const int NumSamplesToUseForCentroid = 15;

	private readonly Dictionary<string, FuzzyVariable> _variables;
	private readonly List<FuzzyRule> _rules;

	public FuzzyModule()
	{
		_rules = new List<FuzzyRule>();
		_variables = new Dictionary<string, FuzzyVariable>();
	}

	private void SetConfidencesOfConsequentsToZero()
	{
		foreach (var rule in _rules)
		{
			rule.SetConfidenceOfConsequentToZero();
		}
	}

	public FuzzyVariable CreateFLV(string varName)
	{
		_variables[varName] = new FuzzyVariable();
		return _variables[varName];
	}

	public void AddRule(FuzzyTerm antecedent, FuzzyTerm consequent)
	{
		_rules.Add(new FuzzyRule(antecedent, consequent));
	}

	public void Fuzzify(string name, double val)
	{
		if (!_variables.ContainsKey(name))
		{
			throw new Exception("Key not found");
		}

		_variables[name].Fuzzify(val);
	}

	public double Defuzzify(string name, DefuzzifyType type)
	{
		if (!_variables.ContainsKey(name))
		{
			throw new Exception("Key not found");
		}

		SetConfidencesOfConsequentsToZero();

		foreach (var rule in _rules)
		{
			rule.Calculate();
		}

		switch (type)
		{
			case DefuzzifyType.Centroid:
				return _variables[name].DefuzzifyCentroid(NumSamplesToUseForCentroid);
			case DefuzzifyType.MaxAv:
				return _variables[name].DefuzzifyMaxAv();
		}

		return 0d;
	}
}