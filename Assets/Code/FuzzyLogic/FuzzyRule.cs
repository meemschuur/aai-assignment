﻿public class FuzzyRule
{
	private readonly FuzzyTerm _antecedent;
	private readonly FuzzyTerm _consequent;

	public FuzzyRule(FuzzyTerm ant, FuzzyTerm con)
	{
		_antecedent = ant;
		_consequent = con;
	}

	public void SetConfidenceOfConsequentToZero()
	{
		_consequent.ClearDOM();
	}

	public void Calculate()
	{
		_consequent.ORwithDOM(_antecedent.DOM);
	}
}