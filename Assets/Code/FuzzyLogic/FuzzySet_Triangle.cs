﻿public class FuzzySet_Triangle : FuzzySet
{
	private readonly double _peakPoint;
	private readonly double _leftOffset;
	private readonly double _rightOffset;

	public FuzzySet_Triangle(double mid, double left, double right) : base(mid)
	{
		_peakPoint = mid;
		_leftOffset = left;
		_rightOffset = right;
	}

	public override double CalculateDOM(double val)
	{
		// Test for the case where the triangle's left or right offsets are zero
		// (to prevent divide by zero errors below).
		if (_rightOffset == 0d && _peakPoint == val ||
		    _leftOffset == 0d && _peakPoint == val)
		{
			return 1d;
		}

		// Find DOM if left of center.
		if (val <= _peakPoint && val >= _peakPoint - _leftOffset)
		{
			double grad = 1d / _leftOffset;

			return grad * (val - (_peakPoint - _leftOffset));
		}

		// Find DOM if right of center.
		if (val > _peakPoint && val < _peakPoint + _rightOffset)
		{
			double grad = 1d / -_rightOffset;
			var calculatedDom = grad * (val - _peakPoint) + 1d;
			return calculatedDom;
		}

		// Out of range of this FLV, return zero.
		return 0d;
	}
}