﻿public class FzSet : FuzzyTerm
{
	private readonly FuzzySet _set;

	public FzSet(FuzzySet fuzzySet)
	{
		_set = fuzzySet;
	}

	public override double DOM
	{
		get { return _set.DOM; }
	}

	public override void ClearDOM()
	{
		_set.ClearDOM();
	}

	public override void ORwithDOM(double val)
	{
		_set.ORwithDOM(val);
	}
}