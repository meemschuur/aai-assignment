﻿using System.Collections.Generic;

public class FuzzyOR : FuzzyTerm
{
	private List<FuzzyTerm> _terms;

	public FuzzyOR(FuzzyTerm op1, FuzzyTerm op2)
	{
		_terms = new List<FuzzyTerm> {op1, op2};
	}

	public FuzzyOR(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3)
	{
		_terms = new List<FuzzyTerm> {op1, op2, op3};
	}

	public FuzzyOR(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3, FuzzyTerm op4)
	{
		_terms = new List<FuzzyTerm> {op1, op2, op3, op4};
	}

	public override double DOM
	{
		get
		{
			var largest = double.MinValue;

			foreach (var term in _terms)
			{
				if (term.DOM > largest)
				{
					largest = term.DOM;
				}
			}

			return largest;
		}
	}

	public override void ClearDOM()
	{
		foreach (var term in _terms)
		{
			term.ClearDOM();
		}
	}

	public override void ORwithDOM(double val)
	{
		foreach (var term in _terms)
		{
			term.ORwithDOM(val);
		}
	}
}