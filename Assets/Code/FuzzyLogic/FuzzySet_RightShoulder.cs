﻿public class FuzzySet_RightShoulder : FuzzySet
{
	private readonly double _peakPoint;
	private readonly double _leftOffset;
	private readonly double _rightOffset;

	public FuzzySet_RightShoulder(double peak, double left, double right) : base((peak + right + peak) / 2)
	{
		_peakPoint = peak;
		_leftOffset = left;
		_rightOffset = right;
	}

	public override double CalculateDOM(double val)
	{
		// Check for case where offset is zero.
		if (_rightOffset == 0d && _peakPoint == val ||
		    _leftOffset == 0d && _peakPoint == val)
		{
			return 1d;
		}

		// Find DOM if left of center.
		if (val <= _peakPoint && val > _peakPoint - _leftOffset)
		{
			var grad = 1d / _leftOffset;
			var calculatedDom = grad * (val - (_peakPoint - _leftOffset));
			return calculatedDom;
		}

		// Find DOM if right of center.
		if (val > _peakPoint)
		{
			return 1d;
		}

		// Out of range of this FLV, return zero.
		return 0d;
	}
}