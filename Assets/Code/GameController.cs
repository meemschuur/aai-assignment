using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The GameController Singleton.
/// </summary>
public class GameController : MonoBehaviour
{
	public static GameController Instance;

	[Header("References")] [SerializeField]
	private GameObject _birdPrefab;

	[SerializeField] private GameObject _predatorPrefab;
	[SerializeField] private Camera _cam;

	[Header("Steering Behaviours")] [Range(0, 400)]
	public int FlockSize = 200;

	public Vector3Int WorldSize = new Vector3Int(200, 100, 75);
	[Range(0, 10)] public int NeighborhoodRadius = 1;

	[SerializeField, Range(0, 1)] private float _cohesionWeight;
	[SerializeField, Range(0, 1)] private float _allignmentWeight;
	[SerializeField, Range(0, 1)] private float _separationWeight;
	[SerializeField, Range(0, 4)] private float _maxSpeed;
	[SerializeField, Range(0, 1)] private float _maxSteerForce;
	[SerializeField] private bool _shouldDrawPath = true;
	[SerializeField] private bool _shouldDrawGrid = true;
	[SerializeField] private bool _paused;
	[SerializeField] private Heuristic _aStarHeuristic = Heuristic.Manhattan;

	#region Dynamic UI properties.

	// These fields need separate properties for the UI sliders to work.
	public float CohesionWeight
	{
		get { return _cohesionWeight; }
		set { _cohesionWeight = value; }
	}

	public float AllignmentWeight
	{
		get { return _allignmentWeight; }
		set { _allignmentWeight = value; }
	}

	public float SeparationWeight
	{
		get { return _separationWeight; }
		set { _separationWeight = value; }
	}

	public float MaxSpeed
	{
		get { return _maxSpeed; }
		set { _maxSpeed = value; }
	}

	public float MaxSteerForce
	{
		get { return _maxSteerForce; }
		set { _maxSteerForce = value; }
	}

	public bool ShouldDrawPath
	{
		get { return _shouldDrawPath; }
		set { _shouldDrawPath = value; }
	}

	public bool ShouldDrawGrid
	{
		get { return _shouldDrawGrid; }
		set { _shouldDrawGrid = value; }
	}

	public bool Paused
	{
		get { return _paused; }
		set { _paused = value; }
	}

	public Heuristic AStarHeuristic
	{
		get { return _aStarHeuristic; }
		set { _aStarHeuristic = value; }
	}

	#endregion

	public Graph Graph { get; private set; }

	private List<Boid> _boids;
	private Vector3 _oldMousePosition;

	private void Awake()
	{
		// Remove GameObject if the Singleton already exists.
		if (Instance != null)
		{
			Destroy(gameObject);
			return;
		}

		Instance = this;
	}

	private void Start()
	{
		Graph = gameObject.GetComponent<Graph>();
		Instantiate(_predatorPrefab);
		_boids = new List<Boid>();
		for (var i = 0; i < FlockSize; i++)
		{
			var boid = Instantiate(_birdPrefab).GetComponent<Boid>();
			boid.ShouldAvoidWalls = true;
			_boids.Add(boid);
		}
	}

	private void Update()
	{
		if (_paused)
		{
			return;
		}

		var mousePosition = Input.mousePosition;
		mousePosition.z = _cam.transform.position.z;
		var repulseLocation = _cam.ScreenToWorldPoint(mousePosition);

		foreach (var boid in _boids)
		{
			Graph.UpdateCell(boid);
			boid.Run();
			if (_oldMousePosition != mousePosition)
			{
				repulseLocation.z = boid.transform.position.z;
				boid.Repulse(repulseLocation);
			}
		}

		_oldMousePosition = mousePosition;
	}

	public Boid RandomBoid()
	{
		return _boids[Random.Range(0, _boids.Count)];
	}
}