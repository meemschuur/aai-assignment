﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Dropdown))]
public class HeuristicDropdown : MonoBehaviour
{
	private void Start()
	{
		// Add the Heuristic Enum as options to the current dropdown.
		GetComponent<Dropdown>().AddOptions(Enum.GetNames(typeof(Heuristic)).ToList());
	}

	public void ChangedDropdownValue(int index)
	{
		GameController.Instance.AStarHeuristic = (Heuristic) index;
	}
}